package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void noParametersConstructor() {
		list = new IntegerList();
		assertThat( list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void addLastTest() {
		list = new IntegerList();
		list.addLast(1);
		assertThat( list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat( list.toString()).isEqualTo("[1 3]");
	}
	
	@Test
	public void stringConstructor(){
		/*Esempio (1), la costruzione di una lista con parametro "" inizializza una lista vuota.
		Esempio (2), se una lista viene inizializzata con parametro "1", il metodo toString restituisce: "[1]".
		Esempio (3), se una lista viene inizializzata con parametro "1 2 3", il metodo toString restituisce: "[1 2 3]".*/
		
		list = new IntegerList("");
		assertThat( list.toString()).isEqualTo("[]");
		list = new IntegerList("1");
		assertThat( list.toString()).isEqualTo("[1]");
		list = new IntegerList("1 2 3");
		assertThat( list.toString()).isEqualTo("[1 2 3]");
		
		
	}
	
}
