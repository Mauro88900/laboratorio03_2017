package it.unimi.di.sweng.lab03;

import java.util.Scanner;

public class IntegerList {
	private IntegerNode head=null;
	
	
	public IntegerList(String numbers) {
		Scanner s = new Scanner(numbers);
		IntegerNode currentNode = null;
		IntegerNode integerFutureNode = null ;
		while(s.hasNextInt()){
			if(this.head==null){
				this.head=new IntegerNode(s.nextInt());
				currentNode = head;
			}
			else{
				currentNode.setNext(integerFutureNode  = new IntegerNode(s.nextInt()));
				currentNode=integerFutureNode;
			}
		}
		s.close();
	}

	public IntegerList() {
	}

	public String toString(){
		StringBuilder result = new StringBuilder("[");
		IntegerNode currentNode=head;
		while (currentNode!=null){
			result.append(currentNode.getValue());
			result.append(" ");
			currentNode=currentNode.getNext();
		}
		if(result.length()>1)
			result.deleteCharAt(result.length()-1);
		result.append("]");
		return result.toString();
	}

	public void addLast(int i) {
		//this.add(i);
		if (head==null){
			this.head = new IntegerNode(i);
		}else{
			IntegerNode currentNode = head;
			while(currentNode.getNext()!=null){
				currentNode=currentNode.getNext();
			}
			currentNode.setNext(new IntegerNode(i));
		}
	}
}

