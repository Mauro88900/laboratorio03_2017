package it.unimi.di.sweng.lab03;

public class IntegerNode {
	private Integer value = null;
	private IntegerNode next = null;

	public IntegerNode(int i) {
		this.value=new Integer(i);
	}

	public IntegerNode getNext() {
		return this.next;
	}

	public void setNext(IntegerNode prossimo) {
		this.next=prossimo;	
	}

	public Integer getValue() {
		return this.value;
	}

}
